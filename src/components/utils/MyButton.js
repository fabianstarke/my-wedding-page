import React from 'react';
import Button from '@material-ui/core/Button';

const MyButton = (props) => {
  return (
    <Button
      href= {props.link}
      variant={props.variant ? props.variant : 'text'}
      size="small"
      onClick={props.onClick}
      style={{
        background: props.bck,
        color: props.color
      }}

    >
      {props.icon}
      {props.text}
    </Button>
  );
};

export default MyButton;
