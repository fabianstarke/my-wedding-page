import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SideDrawer from './SideDrawer';

class Header extends Component {
  state = {
    drawerOpen: false,
    headerShow: false
  }

  componentDidMount(){
      window.addEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    if(window.scrollY > 0){
      this.setState({
        headerShow: true
      })
    } else {
      this.setState({
        headerShow: false
      })
    }
  }

  toggleDrawer = (value) => {
    this.setState({
      drawerOpen: value
    })
  }
  render() {
    return (
      <AppBar
        position = "fixed"
        style = {{
          backgroundColor: this.state.headerShow ?  '#3c3c3c' : "transparent",
          boxShadow: 'none',
          position: 'absolute',
          padding: '10px 0px',
          width: '100%',
        }}
      >
        <Toolbar>
          <div className="header_logo">
            <div className="font_josefin header_logo_venue">Le Jour J</div>
            <div className="header_logo_title font_josefin">Adeline & Fabian</div>
          </div>
          <IconButton
            aria-label="Menu"
            color="inherit"
            onClick={(value) => this.toggleDrawer(value)}
          >
            <MenuIcon />
          </IconButton>
          <SideDrawer
            open={this.state.drawerOpen}
            onClose={(value) => this.toggleDrawer(value)}
          />
        </Toolbar>
      </AppBar>
    );
  }
}

export default Header;
