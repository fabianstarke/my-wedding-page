import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { scroller } from 'react-scroll';

const SideDrawer = (props) => {

  const scrollToElement = (element) => {
    scroller.scrollTo(element, {
      duration: 1500,
      delay: 100,
      smooth: true,
      offset: -100
    });
    props.onClose(false)
  }

  return(
    <Drawer
      anchor="right"
      open={props.open}
      onClose={() => props.onClose(false)}
    >
      <List component="nav">
        <ListItem
          button
          onClick={() => scrollToElement("featured")}
        > Le Jour J
        </ListItem>
        <ListItem
          button
          onClick={() =>  scrollToElement("welcome")}
        > Bienvenue
        </ListItem>
        <ListItem
          button
          onClick={() =>  scrollToElement("venueinfo")}
        > Le programme
        </ListItem>
        <ListItem
          button
          onClick={() =>  scrollToElement("place")}
        > Le lieu
        </ListItem>
        <ListItem
          button
          onClick={() => scrollToElement("highlights")}
        > La chambre
        </ListItem>
        <ListItem
          button
          onClick={() => scrollToElement("pricing")}
        > Profites-en
        </ListItem>
        <ListItem
          button
          onClick={() => scrollToElement("location")}
        > Comment venir
        </ListItem>
      </List>
    </Drawer>
  );
};

export default SideDrawer;
