import React from 'react';
import Fade from 'react-reveal/Fade';

const Footer = () => {
  return (
    <footer className="bck_white">
      <Fade delay={500}>
        <div className="font_josefin footer_logo_venue">
          Le Jour J
        </div>
        <div className="footer_copyright font_josefin">
          <span>Réalisé avec &hearts; par Fabian Starke.</span>
          <br />
          <span>Tous droits réservés.</span>
        </div>
      </Fade>
    </footer>
  )
}

export default Footer
