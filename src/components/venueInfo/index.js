import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Zoom from 'react-reveal/Zoom';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import background1 from '../../ressources/images/background1.jpg'
import { Icon } from 'react-icons-kit'
import {u1F377} from 'react-icons-kit/noto_emoji_regular/u1F377' // verre à vin
import {u1F429} from 'react-icons-kit/noto_emoji_regular/u1F429' // chien
import {u1F454} from 'react-icons-kit/noto_emoji_regular/u1F454' // cravatte
import {u1F492} from 'react-icons-kit/noto_emoji_regular/u1F492' // eglise avec coeurs
import {u1F4CC} from 'react-icons-kit/noto_emoji_regular/u1F4CC' // epingle
import {u1F697} from 'react-icons-kit/noto_emoji_regular/u1F697' // voiture
import { u1F476 } from 'react-icons-kit/noto_emoji_regular/u1F476' // enfant
import { u1F4E6 } from "react-icons-kit/noto_emoji_regular/u1F4E6"; // urne


const styles = theme => ({
  title: {
    fontSize: 34,
    fontWeight: 400,
    textAlign: "justify",
    fontFamily: "Josefin Sans",
    [theme.breakpoints.down('xs')] : {
      fontSize: 20,
      fontWeight: 500
    }
  },
  subheader: {
    fontSize: 24,
    textAlign: "justify",
    fontFamily: "Josefin Sans",
    color: "#607D8B",
    [theme.breakpoints.down('xs')] : {
      fontSize: 14,
      fontWeight: 500
    }
  },
  root: {
    fontWeight: 400,
    fontSize: 28,
    fontFamily: "Josefin Sans",
    [theme.breakpoints.down('xs')] : {
      fontSize: 18,
    }
  },
  expand: {
   transform: 'rotate(0deg)',
   marginLeft: 10,
   paddingRight: 10,
   paddingTop: 15,
   transition: theme.transitions.create('transform', {
     duration: theme.transitions.duration.shortest,
   }),
 },
 expandOpen: {
   transform: 'rotate(180deg)',
 },
 typo: {
   paddingLeft: 10,
   fontFamily: "Josefin Sans",
   [theme.breakpoints.down('xs')] : {
     paddingTop: 0,
     paddingBottom: 0
   },
   [theme.breakpoints.up('sm')] : {
     paddingTop: 10,
     paddingBottom: 10
   },
   [theme.breakpoints.up('lg')] : {
     paddingTop: 20,
     paddingBottom: 20
   }
 },
 cardContent: {
   display: 'flex',
   color: '#F4A261',
   paddingTop: 20,
   paddingBottom: 10
 }
});

class VenueInfo extends React.Component  {
  state = { expanded: false };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render() {
    const { classes } = this.props;
    return (
      <div
        className="carrousel_image"
        style={{
          background: `url(${background1})`
        }}
      >
        <Zoom left duration={1000} delay={1500}>
          <div className="vn_wrapper">
            <div className="vn_item">
              <div className="vn_outer">
                <Card style={{ opacity: 0.9 }}>
                  <CardHeader
                    classes={{
                      title: classes.title,
                      subheader: classes.subheader
                    }}
                    title="Ton Programme"
                    subheader="du 7 septembre 2019"
                    avatar={
                      <Avatar
                        aria-label="Programme"
                        style={{
                          backgroundColor: "#F4A261",
                          width: 64,
                          height: 64
                        }}
                      >
                        <Icon size={48} icon={u1F4CC} />
                      </Avatar>
                    }
                    action={
                      <IconButton
                        className={classnames(classes.expand, {
                          [classes.expandOpen]: this.state.expanded
                        })}
                        onClick={this.handleExpandClick}
                        aria-expanded={this.state.expanded}
                        aria-label="Show more"
                      >
                        <ExpandMoreIcon style={{ fontSize: 48 }} />
                      </IconButton>
                    }
                  />
                  {/*<CardMedia style={{ color: '#F4A261' }}>
                          <Icon size={88} icon={u1F4C3} />
                          <Icon size={88} icon={u1F378} />
                          <Icon size={88} icon={u1F374} />
                          <Icon size={88} icon={u1F386} />
                        </CardMedia> */}
                  <Collapse
                    in={this.state.expanded}
                    timeout="auto"
                    unmountOnExit
                  >
                    <CardContent>
                      <div className={classes.cardContent}>
                        <Icon size={64} icon={u1F492} />
                        <Typography className={classes.typo} paragraph>
                          La cérémonie religieuse se déroulera à 15h à
                          l'église catholique de Barr. Puis nous nous
                          rendrons sur le lieu de réception tous ensembles
                          (environ 10 minutes à pied).
                        </Typography>
                      </div>
                      <div className={classes.cardContent}>
                        <Icon size={64} icon={u1F697} />
                        <Typography className={classes.typo} paragraph>
                          Un grand parking se trouve juste devant l'église.
                          Tu peux donc arriver à l'église en voiture puis la
                          laisser garer pour continuer les festivités.
                        </Typography>
                      </div>
                      <div className={classes.cardContent}>
                        <Icon size={64} icon={u1F377} />
                        <Typography className={classes.typo} paragraph>
                          Nous avons le plaisir de te convier sur notre lieu
                          de réception où se déroulera le vin d'honneur
                          suivi du repas et de la fête.
                        </Typography>
                      </div>
                      <div className={classes.cardContent}>
                        <Icon size={64} icon={u1F454} />
                        <Typography className={classes.typo} paragraph>
                          Il n'y a aucun dress code imposé, viens comme tu
                          es tout simplement. Nous te faisons confiance pour
                          être élégant pour nous !
                        </Typography>
                      </div>
                      <div className={classes.cardContent}>
                        <Icon size={64} icon={u1F4E6} />
                        <Typography className={classes.typo} paragraph>
                          Une urne sera disponible pour nous aider à réaliser nos projets futurs.
                        </Typography>
                      </div>
                      <div className={classes.cardContent}>
                        <Icon size={64} icon={u1F476} />
                        <Typography className={classes.typo} paragraph>
                          Les enfants sont les bienvenus et sous la
                          responsabilité de leurs parents. Si vous souhaitez
                          organiser un encadrement, n'hésitez pas à nous
                          prévenir.
                        </Typography>
                      </div>
                      <div className={classes.cardContent}>
                        <Icon size={64} icon={u1F429} />
                        <Typography className={classes.typo} paragraph>
                          Ton compagnon à quatre pattes sera le bienvenu à
                          l'hôtel mais il ne pourra pas entrer dans l'église
                          ni dans la salle de réception.
                        </Typography>
                      </div>
                    </CardContent>
                  </Collapse>
                </Card>
              </div>
            </div>
          </div>
        </Zoom>
      </div>
    );
  }
};

VenueInfo.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VenueInfo);
