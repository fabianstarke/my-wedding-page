import React from 'react';
import Fade from 'react-reveal/Fade';
import LightSpeed from 'react-reveal/Slide';

import WeddingDress from '../../ressources/images/weddingdress.png'

const Introduction = () => {
  return(
    <div className="center_wrapper">
      <div className="discount_wrapper">
        <Fade>
          <div className="discount_porcentage">
            <img src={WeddingDress} alt="weddingdress" />
          </div>
        </Fade>
        <LightSpeed right cascade>
          <div className="discount_description font_josefin">
            <h3>Bienvenue sur le site de notre mariage</h3>
            <p>Nous avons décidé de créer ce site afin que tu puisses accéder à toutes les informations nécessaires concernant notre mariage.</p>
            <p>Ce site sera évolutif, nous ajouterons au fur et à mesure des informations et nous te tiendrons au courant si nécessaire.</p>
            <p>Après le mariage une galerie sera disponible pour que tu puisses ajouter toutes tes photos et voir celles des autres.</p>
          </div>
        </LightSpeed>
      </div>
    </div>

  )
};

export default Introduction
