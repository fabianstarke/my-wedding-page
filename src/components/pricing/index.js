import React, { Component } from 'react';
import Zoom from 'react-reveal/Zoom'
import Brunch from '../../ressources/images/breakfast.png'
import SPA from '../../ressources/images/spa.png'
import Bed from '../../ressources/images/bed.png'

class Pricing extends Component {

  state = {
      prices: [Brunch,Bed,SPA],
      positions: ["Brunch", "Détente", "SPA"],
      desc: [
        "Un brunch te sera servi entre 10h et 13h30 ! Cela nous permettera de tous nous retrouver le lendemain de la fête et d'étendre la magie de ce moment.",
        "Profite avant tout du moment ! Te préparer dans ta chambre avant la cérémonie et dormir sur place te permettra de faire la fête sans te priver et en toute sécurité. La chambre sera à toi jusqu'à 12h.",
        "Ressources-toi dans l'espace Spa. Tous les équipements nécessaires seront à ta disposition, avant et après la fête. L'accès est également inclus la journée de Dimanche, penses-y !!"
      ],
      delay: [1000,0,1000]
  }

  showBoxes = () => (
    this.state.prices.map((box,i) => (
      <Zoom delay={this.state.delay[i]} key={i}>
      <div className="pricing_item">
        <div className="pricing_inner_wrapper">
          <div className="pricing_title font_josefin">
            <span><img src={this.state.prices[i]} alt={this.state.positions} /></span>
            <span>{this.state.positions[i]}</span>
          </div>
          <div className="pricing_description font_josefin">
            {this.state.desc[i]}
          </div>
        </div>
      </div>
    </Zoom>
    ))
  )

  render(){
    return(
      <div className="bck_black">
        <div className="center_wrapper pricing_section font_josefin">
          <h2>Pourquoi dormir à l'hôtel ?</h2>
          <div className="pricing_wrapper">
            {this.showBoxes()}
          </div>
        </div>
      </div>
    );
  };
}

export default Pricing;
