import React from 'react';
import PropTypes from 'prop-types';

const Panel = (props) => {
  return(
    <div
      className={props.className}
      style={props.style}
      onClick={props.onClick}>
      <p>{props.firstChild}</p>
      <p>{props.secondChild}</p>
      <p>{props.thirdChild}</p>
    </div>
  )
}

Panel.propTypes = {
  onClick: PropTypes.func.isRequired,
  className:PropTypes.any.isRequired,
  firstChild: PropTypes.string.isRequired,
  secondChild: PropTypes.string.isRequired,
  thirdChild: PropTypes.string.isRequired,
};

export default Panel
