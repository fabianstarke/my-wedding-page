import React, { Component } from 'react';
import Panel from './panel'

class ImageGallery extends Component {
  constructor(props){
    super(props)
    this.state = {
      openPanel: undefined,
    }
    }

    toggleOpen = panelNumber => {
    this.setState(prevState => ({
      openPanel: prevState.openPanel === panelNumber ? undefined : panelNumber
    }))
    }

    render() {
    return (
      <div className="panels">
        <Panel
          firstChild="Ca se "
          secondChild="Passe"
          thirdChild="Ici"
          onClick={() => this.toggleOpen(1)}
          className={`panel panel1 ${this.state.openPanel === 1 ?"open open-active" : ""}`}
        />
        <Panel
          firstChild="Découvre"
          secondChild="Ce"
          thirdChild="Lieu"
          onClick={() => this.toggleOpen(2)}
          className={`panel panel2 ${this.state.openPanel === 2 ?"open open-active" : ""}`}
        />
        <Panel
          firstChild="Un"
          secondChild="Moment"
          thirdChild="Fort"
          onClick={() => this.toggleOpen(3)}
          className={`panel panel3 ${this.state.openPanel === 3 ?"open open-active" : ""}`}
        />
        <Panel
          firstChild="Trinque"
          secondChild="Avec"
          thirdChild="Nous"
          onClick={() => this.toggleOpen(4)}
          className={`panel panel4 ${this.state.openPanel === 4 ?"open open-active" : ""}`}
        />
        <Panel
          firstChild="Ca"
          secondChild="Nous"
          thirdChild="Ressemble"
          onClick={() => this.toggleOpen(5)}
          className={`panel panel5 ${this.state.openPanel === 5 ?"open open-active" : ""}`}
        />
      </div>
    )
  };
}

export default ImageGallery;
