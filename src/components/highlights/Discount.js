import React, { Component } from "react";
import Fade from "react-reveal/Fade";
import Slide from "react-reveal/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import withMobileDialog from "@material-ui/core/withMobileDialog";
import MyButton from "../utils/MyButton";
import { Icon } from "react-icons-kit";
import {
  u1F4BC,
  u1F44C,
  u1F4DE,
  u1F4EB,
  u1F468,
  u1F3F0,
  u1F4E7
} from "react-icons-kit/noto_emoji_regular";
import { ic_http } from "react-icons-kit/md/ic_http";

class Discount extends Component {
  state = {
    discountStart: 0,
    discountEnd: 50,
    open: false
  };

  porcentage = () => {
    if (this.state.discountStart < this.state.discountEnd) {
      this.setState({
        discountStart: this.state.discountStart + 1
      });
    }
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  componentDidUpdate() {
    setTimeout(() => {
      this.porcentage();
    }, 10);
  }

  render() {
    const { fullScreen } = this.props;
    return (
      <div className="center_wrapper">
        <div className="discount_wrapper">
          <Fade onReveal={() => this.porcentage()}>
            <div className="discount_porcentage">
              <span>{this.state.discountStart}€</span>
              <br />
              <p style={{ fontSize: 12, textAlign: "center" }}>
                <i>(* par personne en chambre double)</i>
              </p>
            </div>
          </Fade>
          <Slide right>
            <div className="discount_description">
              <h3>Pense à réserver ta chambre</h3>
              <p>
                Nous avons privatisé l'hôtel afin que nous nous sentions
                comme à la maison entourés de tous nos proches. Une chambre
                est donc disponible pour chacun. Cela te permettera de
                t'installer et de profiter pleinement du lieu et de
                l'évènement, ainsi nous pourrons passer 24h tous ensemble.
              </p>
              <p>
                Nous avons prépayé l'intégralité des chambres de l'hôtel. Le
                prix moyen annoncé de 220€ par chambre à cette période étant
                tout de même élevé, nous te demandons une participation de
                50€. Pense à réserver au plus vite afin de garantir ta
                chambre.
              </p>
              <MyButton
                text="Réserve ta chambre"
                variant="contained"
                bck="#F44336"
                color="#ffffff"
                onClick={this.handleClickOpen}
                icon={
                  <Icon size={32} className="iconImage" icon={u1F4BC} />
                }
              />
              <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                fullScreen={fullScreen}
              >
                <DialogTitle>
                  {"Contact pour réserver ta chambre"}
                </DialogTitle>
                <DialogContent>
                  <DialogContentText style={{ paddingBottom: 30 }}>
                    Pour réserver ta chambre il te suffira de contacter par
                    le moyen que tu préfères l'hôtel. Ils te proposeront le
                    tarif préférentiel attribué à nos invités, soit 50€ par
                    personne en chambre double et un supplément de 30€ en
                    chambre simple.
                  </DialogContentText>
                  <div style={{ paddingBottom: 30 }}>
                    <span>
                      <Icon className="iconImage" size={32} icon={u1F3F0} />
                    </span>
                    <span>5TERRES Hôtel & Spa</span>>
                  </div>
                  <div style={{ paddingBottom: 30 }}>
                    <span>
                      <Icon
                        className="iconImage"
                        size={32}
                        icon={ic_http}
                      />
                    </span>
                    <span>
                      <a href="https://www.5terres-hotel.fr/fr/">
                        https://www.5terres-hotel.fr/fr/
                      </a>
                    </span>
                  </div>
                  <div style={{ paddingBottom: 10 }}>
                    <span>
                      <Icon className="iconImage" size={32} icon={u1F468} />
                    </span>
                    <span>Jean-Baptiste Zekkout</span>
                  </div>
                  <div style={{ paddingBottom: 10 }}>
                    <span>
                      <Icon className="iconImage" size={32} icon={u1F4DE} />
                    </span>
                    <span>+ 33 (0)3 88 08 28 44</span>
                  </div>
                  <div style={{ paddingBottom: 10 }}>
                    <span>
                      <Icon className="iconImage" size={32} icon={u1F4E7} />
                    </span>
                    <span>HA7F3-SB@accor.com</span>
                  </div>
                  <div style={{ paddingBottom: 10 }}>
                    <span>
                      <Icon className="iconImage" size={32} icon={u1F4EB} />
                    </span>
                    <span>11 place de l’Hôtel de Ville / 67140 Barr</span>
                  </div>
                  <div style={{ paddingTop: 30 }}>
                    <p>
                      <i>
                        (Tout supplément éventuel relatif à un animal de
                        compagnie, lit supplémentaire ou autres frais à la
                        charge de chacun)
                      </i>
                    </p>
                  </div>
                </DialogContent>
                <DialogActions>
                  <MyButton
                    text="J'ai tout compris"
                    color="#8BC34A"
                    onClick={this.handleClose}
                    icon={
                      <Icon size={32} className="iconImage" icon={u1F44C} />
                    }
                  />
                </DialogActions>
              </Dialog>
            </div>
          </Slide>
        </div>
      </div>
    );
  }
}

export default withMobileDialog()(Discount);
