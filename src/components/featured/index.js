import React from 'react';
import Caroussel from './Caroussel';
import TimeUntil from './TimeUntil';
import MyButton from '../utils/MyButton';
import { Icon } from "react-icons-kit";
import { u1F449 } from "react-icons-kit/noto_emoji_regular/u1F449";
import { scroller } from "react-scroll";

const Featured = (props) => {

  const scrollToElement = element => {
    scroller.scrollTo(element, {
      duration: 1500,
      delay: 100,
      smooth: true,
      offset: -100
    });
  };

  return (
    <div style={{ position: "relative" }}>
      <Caroussel />
      <TimeUntil />
      <div
        className="scroll"
        style={{ position: "absolute", bottom: 10, right: 60 }}
      >
        <MyButton
          text="Voir plus"
          color="#fff"
          onClick={() => scrollToElement("welcome")}
          icon={
            <Icon
              style={{ paddingRight: 40 }}
              size={48}
              className="iconImage"
              icon={u1F449}
            />
          }
        />
      </div>
    </div>
  );
};

export default Featured;
