import React from 'react';
import Slider from "react-slick";

const Caroussel = () => {
  const settings = {
    dots: false,
    infinite: true,
    autoplay: true,
    speed: 1500,
  }

  return (
    <div
      className="carrousel_wrapper"
      style={{
        height: `${window.innerHeight}px`,
        overflow: "hidden"
      }}
    >
      <Slider {...settings}>
        <div>
          <div
            className="carrousel_image3"
            style ={{
              height: `${window.innerHeight}px`,
            }}
          ></div>
        </div>
        <div>
          <div
            className="carrousel_image1"
            style ={{
              height: `${window.innerHeight}px`
            }}
          ></div>
        </div>
        <div>
          <div
            className="carrousel_image4"
            style ={{
              height: `${window.innerHeight}px`,

            }}
          ></div>
        </div>
        <div>
          <div
            className="carrousel_image5"
            style ={{
              height: `${window.innerHeight}px`,

            }}
          ></div>
        </div>
      </Slider>
    </div>
  );
}

export default Caroussel;
