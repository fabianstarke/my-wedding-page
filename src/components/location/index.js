import React from 'react';

const Location = () => {
  return (
    <div className="location_wrapper">
      <iframe
       src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1862.8850397226333!2d7.449168559689141!3d48.40811384026957!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479152108afe7a6d%3A0xdf4decd87aef4b65!2s5+Terres+H%C3%B4tel+%26+Spa!5e1!3m2!1sfr!2sfr!4v1542643724022"
       title="google map"
       width="100%"
       height="500px"
       frameBorder="0"
       allowFullScreen
     >
      </iframe>
      <div className="location_tag font_josefin">
        <div>
          Y aller ?
        </div>
      </div>
    </div>
  )
}

export default Location;
