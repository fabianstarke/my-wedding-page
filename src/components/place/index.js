import React from 'react';
import Bounce from 'react-reveal/Bounce';

const Place = () => {
  return(
    <Bounce>
      <div className="highlight_wrapper">
        <div className="center_wrapper font_josefin">
          <h2>Le Lieu</h2>
          <div className="highlight_description font_josefin">
            <p>Nous avons eu le coup de coeur pour ce petit hôtel très authentique situé à Barr, charmant village se trouvant sur la fameuse route des vins d'Alsace.</p>
            <p>Cet hôtel nous représente bien car il fait référence à notre rencontre à l'école hôtelière de Strasbourg et à notre parcours.</p>
            <p>Ce lieu emblématique de l'histoire de la ville est un ancien Bürgerstube de l'époque de la Renaissance qui a récemment été rénové.</p>
            <p>L'architecte s'est inspiré de la nature et on retrouve donc beaucoup de bois brut et du savoir-faire avec des poutres apparentes et des murs en pierres d'époque.</p>
          </div>
        </div>
      </div>
    </Bounce>

  )
};

export default Place
