import React, { Component } from 'react';
import './ressources/styles.css';
import { Element } from 'react-scroll';
import ReactGA from 'react-ga';


import Header from './components/header-footer/Header';
import Introduction from './components/introduction';
import Featured from './components/featured';
import VenueInfo from './components/venueInfo';
import Place from './components/place';
import ImageGallery from './components/imageGallery'
import Highlights from './components/highlights';
import Pricing from './components/pricing';
import Location from './components/location';
import Footer from './components/header-footer/Footer'

const initializeReactGA = () => {
  ReactGA.initialize('UA-135689226-1');
  ReactGA.pageview('/homepage');
}

class App extends Component {

  componentDidMount(){
    initializeReactGA()
  }
  render() {
    return (
      <div className="App">
        <Header />
        <Element name="featured">
          <Featured/>
        </Element>
        <Element name="welcome">
          <Introduction />
        </Element>
        <Element name="venueinfo">
          <VenueInfo/>
        </Element>
        <Element name="place">
          <Place />
        </Element>
        <Element name="ImageGallery">
          <ImageGallery />
        </Element>
        <Element name="highlights">
          <Highlights/>
        </Element>
        <Element name="pricing">
          <Pricing/>
        </Element>
        <Element name="location">
          <Location/>
        </Element>
        <Footer />
      </div>
    );
  }
}

export default App;
