# my-wedding-page

I created this static web page for my wedding.
The goal was that invited people could get some additional infos, have an itenirary to the venue, some story telling and also that after to the wedding everyone could  upload  pictures from the wedding, 

## Deployment

In order to deploy the last changes you did on the repository, use the following command line:

```
yarn deploy
```

Once deployed the terminal will display the new URL of the deployed application.
